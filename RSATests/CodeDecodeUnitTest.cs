using EncryptQR;
using FluentAssertions;
using Xunit;

namespace RSATests
{
    public class CodeDecodeUnitTest : BaseTest
    {
        [Fact]
        public void Shuld_be_equivalent_()
        {
            string value = "salam iran";
            string encResult = RsaEncrypt.Encrypt(PublicKey, value);
            string decResult = RsaDecrypt.Decrypt(PrivateKey, encResult);

            decResult.Should().BeEquivalentTo(value);
        }
    }
}
