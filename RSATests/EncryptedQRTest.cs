﻿using EncryptQR;
using FluentAssertions;
using System;
using Xunit;

namespace RSATests
{
    public class EncryptedQRTest : BaseTest
    {
        string basePath = Environment.CurrentDirectory + "\\qrcodes\\";

        [Fact]
        public void GenerateEncryptedQrCodeBasedOnCustomeModel()
        {
            string qr = JSON.Serialize(QrModel.Generate());
            string key = AESEncrypt.GenerateKey();
            string aesCoded = AESEncrypt.Encrypt(qr, key);
            string aesDecoded = AESDecrypt.Decrypt(aesCoded, key);
            QrCode.Generate(JSON.Serialize(new { Url = "http://saiedkia.ir", Data = aesCoded }), basePath + Guid.NewGuid() + ".jpg");


            aesDecoded.Should().BeEquivalentTo(qr);
        }
    }
}
