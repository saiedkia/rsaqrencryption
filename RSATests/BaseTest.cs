﻿using System;
using System.IO;
using System.Text;

namespace RSATests
{
    public class BaseTest
    {
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }

        public BaseTest()
        {
            string path = Environment.CurrentDirectory;
            if (!File.Exists(path + "\\Key.pub"))
            {
                (PrivateKey, PublicKey) = EncryptQR.RsaGenerator.GenerateRSAKey();
                File.WriteAllText(path + "\\key.pub", PublicKey, Encoding.UTF8);
                File.WriteAllText(path + "\\key.priv", PrivateKey, Encoding.UTF8);
            }
            else
            {
                PublicKey = File.ReadAllText(path + "\\key.pub");
                PrivateKey = File.ReadAllText(path + "\\key.priv");
            }
        }
    }



}
