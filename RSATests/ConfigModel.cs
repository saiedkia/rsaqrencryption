﻿namespace RSATests
{
    public class ConfigModel
    {
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }
    }
}
