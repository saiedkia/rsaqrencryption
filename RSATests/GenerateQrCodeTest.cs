﻿using EncryptQR;
using FluentAssertions;
using System;
using System.IO;
using Xunit;

namespace RSATests
{
    public class GenerateQrCodeTest
    {
        string basePath = Environment.CurrentDirectory + "\\qrcodes\\";

        [Fact]
        public void ClearQrCodeFolder()
        {
            if (Directory.Exists(basePath))
                foreach (string item in Directory.GetFiles(basePath))
                    File.Delete(item);
            else
                Directory.CreateDirectory(basePath);

            string[] files = Directory.GetFiles(basePath);
            files.Should().BeEmpty();
        }


        [Fact]
        public void GenerateSimpleQrCode()
        {
            string path = basePath + DateTime.Now.Millisecond.ToString() + ".png";
            QrCode.Generate("salam iran", path);

            bool isFileGenerated = File.Exists(path);
            isFileGenerated.Should().BeTrue();
        }
    }
}
