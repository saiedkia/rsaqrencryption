﻿using Newtonsoft.Json;

namespace EncryptQR
{
    public static class JSON
    {
        public static string Serialize(object obj) => JsonConvert.SerializeObject(obj);
        public static T Deserialize<T>(string obj) => JsonConvert.DeserializeObject<T>(obj);
    }
}
