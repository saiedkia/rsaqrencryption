﻿using NETCore.Encrypt;

namespace EncryptQR
{
    public static class RsaDecrypt
    {
        public static string Decrypt(string privateKey, string value)
        {
            return EncryptProvider.RSADecrypt(privateKey, value);
        }
    }
}
