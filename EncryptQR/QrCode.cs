﻿using QRCoder;
using System.Drawing;

namespace EncryptQR
{
    public static class QrCode
    {
        public static bool Generate(string value, string path)
        {
            try
            {
                QRCodeGenerator qgenerator = new QRCodeGenerator();
                QRCodeData qdata = qgenerator.CreateQrCode(value, QRCodeGenerator.ECCLevel.M);
                QRCode qrCode = new QRCode(qdata);
                Bitmap bitMap = qrCode.GetGraphic(20);

                bitMap.Save(path);

            }
            catch
            {
                return false;
            }

            return true;
        }


        public static bool Generate<T>(T value, string path)
        {
            return Generate(JSON.Serialize(value), path);
        }
    }
}
