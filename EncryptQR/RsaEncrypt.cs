﻿using NETCore.Encrypt;

namespace EncryptQR
{
    public static class RsaEncrypt
    {
        public static string Encrypt(string publicKey, string value)
        {
            return EncryptProvider.RSAEncrypt(publicKey, value);
        }

        //public static byte[] Encrypt(string publicKey, byte[] value, string initVector)
        //{
        //    return EncryptProvider.RSAEncrypt(value, publicKey, initVector);
        //}
    }
}
