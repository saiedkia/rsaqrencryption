﻿using NETCore.Encrypt;
using NETCore.Encrypt.Internal;

namespace EncryptQR
{
    public static class RsaGenerator
    {
        public static (string privateKey, string publicKey) GenerateRSAKey()
        {
            RSAKey key = EncryptProvider.CreateRsaKey(RsaSize.R2048);
            return (key.PrivateKey, key.PublicKey);
        }
    }
}
